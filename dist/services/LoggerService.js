"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var _a;
Object.defineProperty(exports, "__esModule", { value: true });
exports.logger = void 0;
const pino_1 = __importDefault(require("pino"));
const express_pino_logger_1 = __importDefault(require("express-pino-logger"));
class LoggerService {
}
_a = LoggerService;
LoggerService.info = (message = "", _filename = "") => {
    _a.displayLog(message, _filename, "info");
};
LoggerService.error = (message = "", _filename = "") => {
    _a.displayLog(message, _filename, "error");
};
LoggerService.warn = (message = "", _filename = "") => {
    _a.displayLog(message, _filename, "warn");
};
LoggerService.log = (message = "", _filename = "") => {
    const content = { message, fichier: (_filename !== "" ? _a.getFileRelativePath(_filename) : "Fichier non spécifié.") };
    _a.logToFile.info(content);
};
LoggerService.displayLog = (message, _filename, level) => {
    const content = "\n Message : " + message + " \n Fichier : " + (_filename !== "" ? _a.getFileRelativePath(_filename) : "Fichier non spécifié.");
    _a.logger[level](content);
};
LoggerService.logger = (0, pino_1.default)({
    name: "Logs",
    enabled: true,
    transport: {
        target: "pino-pretty",
        options: {
            translateTime: "SYS: dd-mm-yyyy HH:MM:ss",
            ignore: "pid, hostname"
        }
    }
});
LoggerService.logToFile = (0, pino_1.default)({
    level: "info",
    name: "Logs",
    transport: {
        target: "pino-pretty",
        options: {
            translateTime: "SYS: dd-mm-yyyy HH:MM:ss",
            ignore: "pid, hostname",
            hideObject: false,
            destination: process.cwd() + "/logs/app.log"
        }
    }
});
LoggerService.expressPinoLogger = express_pino_logger_1.default;
LoggerService.getFileRelativePath = (_filename = "") => {
    return (_filename.includes("dist") ? "dist" + _filename.split("/dist").pop() : "src" + _filename.split("/src").pop());
};
exports.logger = LoggerService;
//# sourceMappingURL=LoggerService.js.map