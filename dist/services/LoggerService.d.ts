import pino from 'pino';
declare class LoggerService {
    static info: (message?: string, _filename?: string) => void;
    static error: (message?: string, _filename?: string) => void;
    static warn: (message?: string, _filename?: string) => void;
    static log: (message?: string, _filename?: string) => void;
    static displayLog: (message: string, _filename: string, level: string) => void;
    static logger: pino.Logger;
    static logToFile: pino.Logger;
    static expressPinoLogger: any;
    static getFileRelativePath: (_filename: string) => string;
}
export declare const logger: typeof LoggerService;
export {};
