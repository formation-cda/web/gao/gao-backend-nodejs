"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const ErrorMessage = {
    BAD_REQUEST: "Requête érronée",
    WRONG_PASSWORD: "Mots de passe incorrect",
    WRONG_LOGIN_OR_PASSWORD: "Identifiant ou motes de passe incorrect"
};
exports.default = ErrorMessage;
//# sourceMappingURL=ErrorMessage.js.map