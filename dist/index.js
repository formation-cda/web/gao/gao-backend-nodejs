"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const SuccessMessage_1 = __importDefault(require("./enum/SuccessMessage"));
const LoggerService_1 = require("./services/LoggerService");
const pinoMiddleware = LoggerService_1.logger.expressPinoLogger();
const app = (0, express_1.default)();
const port = 8080; // default port to listen
app.use(pinoMiddleware);
// define a route handler for the default home page
app.get("/", (req, res) => {
    req.log.info('something else');
    res.send("Hello world!");
});
// start the Express server
app.listen(port, () => {
    // console.log( `server started at http://localhost:${ port }` )
    LoggerService_1.logger.info(SuccessMessage_1.default.SUCCESS_CONNEXION, __filename);
});
//# sourceMappingURL=index.js.map