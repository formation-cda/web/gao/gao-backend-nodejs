import { QueryInterface, Sequelize, Op } from 'sequelize'
import { passwordHelper } from '../helper/PasswordHelper'


export = {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */    
    await queryInterface.bulkInsert('clients', [
      {
        id: 1,
        firstname: 'Daniella',
        lastname: 'MICHELLE',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        firstname: 'Dexter',
        lastname: 'MORGAN',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        firstname: 'Cedrick',
        lastname: 'MICHEL',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('clients', { truncate: true, cascade: true });
  }
};
