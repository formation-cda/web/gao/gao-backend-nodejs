import { QueryInterface, Sequelize, Op } from 'sequelize'
import { passwordHelper } from '../helper/PasswordHelper'


export = {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */    
    await queryInterface.bulkInsert('postes', [
      {
        id: 1,
        name: 'PC1',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        name: 'PC2',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        name: 'PC3',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 4,
        name: 'PC4',
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 5,
        name: 'PC5',
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('postes', { truncate: true, cascade: true }
   );
  }
};
