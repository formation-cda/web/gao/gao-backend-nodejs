import { QueryInterface, Sequelize, Op } from 'sequelize'
import { passwordHelper } from '../helper/PasswordHelper'


export = {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */    
    await queryInterface.bulkInsert('attributions', [
      {
        id: 1,
        date: "2022-01-17",
        horaire: '8',
        clientId: 1,
        posteId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        date: "2022-01-18",
        horaire: '9',
        clientId: 2,
        posteId: 2,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        date: "2022-01-17",
        horaire: '10',
        clientId: 3,
        posteId: 1,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('attributions', { truncate: true, cascade: true });
  }
};
