import { QueryInterface, Sequelize, Op } from 'sequelize'
import { passwordHelper } from '../helper/PasswordHelper'


export = {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    const hashedPassword: any = await passwordHelper.hashPassword('Password1,')
    await queryInterface.bulkInsert('users', [
      {
        id: 1,
        name: 'Peter BISHOP',
        email: 'emailbidon1@mailbidon.com',
        password: hashedPassword,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 2,
        name: 'Walter BISHOP',
        email: 'emailbidon2@mailbidon.com',
        password: hashedPassword,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        id: 3,
        name: 'Sherlock HOLMES',
        email: 'emailbidon3@mailbidon.com',
        password: hashedPassword,
        createdAt: new Date(),
        updatedAt: new Date()
      },
    ], {});
  },

  down: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
     await queryInterface.bulkDelete('users',
     {
       [Op.or]: [
         {email: 'emailbidon1@mailbidon.com'},
         {email: 'emailbidon2@mailbidon.com'},
         {email: 'emailbidon3@mailbidon.com'}
       ]
     }
   );
  }
};
