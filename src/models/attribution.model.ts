import { Table, Column, Model, ForeignKey, BelongsTo, CreatedAt, DeletedAt, UpdatedAt, DataType } from 'sequelize-typescript'
import * as Sequelize from 'sequelize'
import Poste from './poste.model'
import Client from './client.model'

@Table({
    tableName: 'attributions'
  })
export default class Attribution extends Model {
  @Column({
      type:DataType.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    })
  id: number

  @Column({
      type:DataType.STRING,
      allowNull: false
    })
  date: string

  @Column({
      type:DataType.STRING,
      allowNull: false
    })
  horaire: string

  @ForeignKey(() => Poste)
  @Column({
      type:DataType.INTEGER,
      allowNull: false
    })
  posteId: number

  @BelongsTo(() => Poste)
  poste: Poste


  @ForeignKey(() => Client)
  @Column({
      type:DataType.INTEGER,
      allowNull: false
    })
  clientId: number

  @BelongsTo(() => Client)
  client: Client

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @DeletedAt
  deletedAt: Date;
}