import { Table, Column, Model, HasMany, CreatedAt, DeletedAt, UpdatedAt, DataType } from 'sequelize-typescript'
import Attribution from './attribution.model'

@Table({
    tableName: 'postes'
  })
export default class Poste extends Model {
  @Column({
      type:DataType.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    })
  id: number

  @Column({
      type:DataType.STRING,
      allowNull: false
    })
  name: string

  @HasMany(() => Attribution, {
    onDelete: 'CASCADE',
    onUpdate: 'CASCADE'
  })
  attributions: Attribution[]

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @DeletedAt
  deletedAt: Date;
}