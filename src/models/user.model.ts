import { Table, Column, Model, ForeignKey, BelongsTo, CreatedAt, DeletedAt, UpdatedAt, DataType } from 'sequelize-typescript'
import * as Sequelize from 'sequelize'

@Table({
    tableName: 'users'
  })
export default class User extends Model {
  @Column({
      type:DataType.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true
    })
  id: number

  @Column({
      type:DataType.STRING,
      allowNull: false
    })
  name: string

  @Column({
      type:DataType.STRING,
      allowNull: false
    })
  email: string

  @Column({
      type:DataType.STRING,
      allowNull: false
    })
  password: string

  @CreatedAt
  createdAt: Date;

  @UpdatedAt
  updatedAt: Date;

  @DeletedAt
  deletedAt: Date;
}

export interface UserAddModel {
    name: string
    email: string
    password: string
}

export interface UserModel extends Sequelize.Model<UserModel, UserAddModel> {
    id: number
    name: string
    email: string
    password: string
    createdAt: string
    updatedAt: string
}

export interface UserViewModel {
    id: number
    name: string
    email: string
}