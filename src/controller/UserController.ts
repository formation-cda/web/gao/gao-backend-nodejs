import User, { UserModel, UserAddModel, UserViewModel } from '../models/user.model'
import { passwordHelper } from '../helper/PasswordHelper'
import BaseController from './BaseController';
import { authHelper } from '../helper/AuthHelper';
import SuccessMessage from '../enum/SuccessMessage';
import ErrorMessage from '../enum/ErrorMessage';
import { Request } from '../helper/Request'
import { Response } from '../helper/Response';
// import QuerySelectionHelper from '../helper/QuerySelectionHelper';

export class UserController extends BaseController {

    // private querySlection = new QuerySelectionHelper()

    async register() {
        const { email, password, name }: any = this.req.body
        let user: any =  null

        const hashedPassword: any = await passwordHelper.hashPassword(password)
        try {
            user = await User.create({ email, name, password: hashedPassword })
        } catch (error) {
            return this.sendError(ErrorMessage.USER_REGISTRATION_FAILED, error)
        }

        if(!user) return this.sendError(ErrorMessage.USER_REGISTRATION_FAILED, [])

        return this.sendResponse(user, SuccessMessage.SUCCESS_USER_REGISTRATION)
    }

    async login() {
        const { email, password }: UserAddModel = this.req.body
        let user: any = null
        // const attributes: any = this.querySlection.exclude(['roleId']).attributes()
        try {
            user = await User.findOne({
                where: { email },
                // attributes
            })
          } catch (error) {
            return this.sendError(ErrorMessage.USER_LOGIN_FAILED, error)
        }

        if(user) {
            const id = user.id!
            const mail = user.email!
            const role = user.role!
            const storedPassword = user.password!
            const pawdVerified: boolean = await passwordHelper.comparePassword(password, storedPassword)
            if(pawdVerified) {
                const token: string = await authHelper.createToken({userId: id, email: mail})
                return this.sendResponse( {user, token}, SuccessMessage.SUCCESS_CONNEXION)
            }
            return this.sendError(ErrorMessage.WRONG_PASSWORD, [])
        }

        return this.sendError(ErrorMessage.WRONG_LOGIN, [])
    }
}

export const userController = (req: Request, res: Response) => {
    return new UserController(req, res)
}