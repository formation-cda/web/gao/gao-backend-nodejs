import BaseController from './BaseController'
import SuccessMessage from "../enum/SuccessMessage"
import { Request } from '../helper/Request'
import { Response } from '../helper/Response';
import Poste from '../models/poste.model'
import QuerySelectionHelper from '../helper/QuerySelectionHelper';
import ErrorMessage from '../enum/ErrorMessage';
import Attribution from '../models/attribution.model';
import Client from '../models/client.model';

class PosteController extends BaseController {

    private querySlection = new QuerySelectionHelper()

    async getPosteById() {
        const paramsStr: any = this.req.query.params
        const params: any = paramsStr ? JSON.parse(paramsStr) : {}
        let poste: any = null        
        const association: any = this.querySlection.join(Attribution, ['id', 'name']).endJoin()
        try {
            poste = await Poste.findOne({
                where: { id: params.posteId },
                include: association,
            })
          } catch (error) {
            return this.sendError(ErrorMessage.ERROR_GET_POSTE, error)
        }
        return this.sendResponse(poste, SuccessMessage.SUCCESS_GET_POSTE)
    }

    async getNbPoste() {
        let nbPoste: any = null        
        try {
            nbPoste = await Poste.count()
          } catch (error) {
            return this.sendError(ErrorMessage.ERROR_GET_POSTE_NB, error)
        }
        return this.sendResponse(nbPoste, SuccessMessage.SUCCESS_GET_POSTE_NB)
    }


    async getListOrdis() {
        let poste: any = null
        const paramsStr: any = this.req.query.params
        const params: any = paramsStr ? JSON.parse(paramsStr) : {}
        const limit = 3;
        const association: any = this.querySlection.join(Attribution, ['id', 'date', 'horaire'], { date: params.date }).nestJoin(Attribution, Client, ['id', 'lastName', 'firstName']).endJoin()
        // console.log("Association : ", ...association)
        try {
            poste = await Poste.findAll({
                include: association,
                offset: Number(params.offset), 
                limit,
                order: [
                    ['name', 'ASC'],
                ]                
            })
          } catch (error) {
            return this.sendError(ErrorMessage.ERROR_GET_POSTES, error)
        }
        
        return this.sendResponse(poste, SuccessMessage.SUCCESS_GET_POSTES)
    }

    async add() {
        const { name }: any = this.req.body
        let poste: any =  null

        try {
            poste = await Poste.create({ name })
        } catch (error) {
            return this.sendError(ErrorMessage.POSTE_ADD_FAILED, error)
        }

        if(!poste) return this.sendError(ErrorMessage.POSTE_ADD_FAILED, [])

        return this.sendResponse(poste, SuccessMessage.SUCCESS_POSTE_ADD)
    }

    async update() {
        const { id, firstname, lastname, email }: any = this.req.body
        let res: any = null
        let data: any = null
        const attributes: any = this.querySlection.include(['id', 'firstname', 'lastname', 'email']).attributes()
        try {
            res = await Poste.update(
                { id, firstname, lastname, email },
                { where: { id } }
            )
            data = await Poste.findOne({
                where: { id },
                attributes
            })
        } catch (error) {
            return this.sendError(ErrorMessage.UPDATE_POSTE_FAILED, error)
        }

        if(res === 0) return this.sendError(ErrorMessage.UPDATE_POSTE_FAILED, [])
        return this.sendResponse(data, SuccessMessage.SUCCESS_UPDATE_POSTE)
    }

    async delete() {
        const { id }: any = this.req.body
        let res: number = 0
        try {
            res = await Poste.destroy({
                where: { id }
            })
          } catch (error) {
            return this.sendError(ErrorMessage.DELETE_POSTE_FAILED, error)
        }
        if(res === 0) return this.sendError(ErrorMessage.DELETE_POSTE_FAILED, [])
        return this.sendResponse(res, SuccessMessage.SUCCESS_DELETE_POSTE)
    }
}

export const posteController = (req: Request, res: Response) => {
    return new PosteController(req, res)
}