import { passwordHelper } from '../helper/PasswordHelper'
import BaseController from './BaseController';
import SuccessMessage from '../enum/SuccessMessage';
import ErrorMessage from '../enum/ErrorMessage';
import { Request } from '../helper/Request'
import { Response } from '../helper/Response';
import Attribution from '../models/attribution.model';
import QuerySelectionHelper from '../helper/QuerySelectionHelper';
import Client from '../models/client.model';

export class AttributionController extends BaseController {
    private querySlection = new QuerySelectionHelper()
  
    async add() {
        const { clientId, posteId, date, horaire }: any = this.req.body
        let res: any =  null
        let attribution: any =  null

        try {
            res = await Attribution.create({ clientId, posteId, date, horaire })
            const association: any = this.querySlection.join(Client, ['id', 'lastName', 'firstName']).endJoin()
            try {
                attribution = await Attribution.findOne({
                    where: { id: res.id},
                    include: association,
                })
            } catch (error) {
                return this.sendError(ErrorMessage.ERROR_GET_ATTRIBUTION, error)
            }
        } catch (error) {
            return this.sendError(ErrorMessage.ADD_ATTRIBUTION_FAILED, error)
        }

        if(!attribution) return this.sendError(ErrorMessage.ADD_ATTRIBUTION_FAILED, [])
        
        return this.sendResponse(attribution, SuccessMessage.SUCCESS_ADD_ATTRIBUTION)
    }

    async delete() {
        const { id }: any = this.req.body
        let res: number = 0
        try {
            res = await Attribution.destroy({
                where: { id }
            })
          } catch (error) {
            return this.sendError(ErrorMessage.DELETE_ATTRIBUTION_FAILED, error)
        }
        if(res === 0) return this.sendError(ErrorMessage.DELETE_ATTRIBUTION_FAILED, [])
        return this.sendResponse(res, SuccessMessage.SUCCESS_DELETE_ATTRIBUTION)
    }
}

export const attributionController = (req: Request, res: Response) => {
    return new AttributionController(req, res)
}