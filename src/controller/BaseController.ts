import { Response } from '../helper/Response';
import { Request } from '../helper/Request';
import { IsArray } from 'sequelize-typescript';

class BaseController {
    public res: Response = null
    public req: Request = null

    constructor(req: Request, res: Response) {
        this.req = req
        this.res = res
    }

    public sendResponse = (result: any = [], message: string) => {
    	const response: any = {
            'success': true,
            'data'   : result,
            'message': message,
        }

        return this.res.status(200).json(response);
    }

    public sendError = (errorMessages: string = "", error: any = [], code: number = 404) => {
    	const response: any = {
            'success': false,
            'message': errorMessages,
        }

        if(error){
            response.data = error;
        }

        return this.res.status(code).json(response);
    }
}

export default BaseController