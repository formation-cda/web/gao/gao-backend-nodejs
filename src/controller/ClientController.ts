import BaseController from './BaseController'
import SuccessMessage from "../enum/SuccessMessage"
import { Request } from '../helper/Request'
import { Response } from '../helper/Response'
import ErrorMessage from '../enum/ErrorMessage'
import Client from '../models/client.model'
import QuerySelectionHelper from '../helper/QuerySelectionHelper'
import { Op } from "sequelize"

class ClientController extends BaseController {
    private querySlection = new QuerySelectionHelper()

    async getClientById() {
        const { clientId } = this.req.params
        let client: any = null        
        try {
            client = await Client.findOne({
                where: { id: clientId }
            })
          } catch (error) {
            return this.sendError(ErrorMessage.ERROR_GET_CLIENT, error)
        }
        return this.sendResponse(client, SuccessMessage.SUCCESS_GET_CLIENT)
    }

    async getClients() {
        let client: any = null
        try {
            client = await Client.findAll()
          } catch (error) {
            return this.sendError(ErrorMessage.ERROR_GET_CLIENTS, error)
        }
        return this.sendResponse(client, SuccessMessage.SUCCESS_GET_CLIENTS)
    }

    async search() {       
        let client: any = null

        const { word } = this.req.body
        try {
            client = await Client.findAll({
                where: {
                    [Op.or]: [
                        {
                            firstName: {
                                [Op.like]: `%${word}%`
                            }
                        },
                        {
                            lastName: {
                                [Op.like]: `%${word}%`
                            }
                        }
                    ]
                }
            })
    
          } catch (error) {
            return this.sendError(ErrorMessage.ERROR_GET_CLIENTS, error)
        }
        return this.sendResponse(client, SuccessMessage.SUCCESS_GET_CLIENTS)
    }

    async add() {
        const { firstName, lastName }: any = this.req.body
        let poste: any =  null

        try {
            poste = await Client.create({ firstName, lastName })
        } catch (error) {
            return this.sendError(ErrorMessage.CLIENT_ADD_FAILED, error)
        }

        if(!poste) return this.sendError(ErrorMessage.CLIENT_ADD_FAILED, [])

        return this.sendResponse(poste, SuccessMessage.SUCCESS_CLIENT_ADD)
    }

    async update() {
        const { id, firstname, lastname, email }: any = this.req.body
        let res: any = null
        let data: any = null
        const attributes: any = this.querySlection.include(['id', 'firstname', 'lastname', 'email']).attributes()
        try {
            res = await Client.update(
                { id, firstname, lastname, email },
                { where: { id } }
            )
            data = await Client.findOne({
                where: { id },
                attributes
            })
        } catch (error) {
            return this.sendError(ErrorMessage.UPDATE_CLIENT_FAILED, error)
        }

        if(res === 0) return this.sendError(ErrorMessage.UPDATE_CLIENT_FAILED, [])
        return this.sendResponse(data, SuccessMessage.SUCCESS_UPDATE_CLIENT)
    }

    async delete() {
        const { id }: any = this.req.body
        let res: number = 0
        try {
            res = await Client.destroy({
                where: { id }
            })
          } catch (error) {
            return this.sendError(ErrorMessage.DELETE_CLIENT_FAILED, error)
        }
        if(res === 0) return this.sendError(ErrorMessage.DELETE_CLIENT_FAILED, [])
        return this.sendResponse(res, SuccessMessage.SUCCESS_DELETE_CLIENT)
    }
}

export const clientController = (req: Request, res: Response) => {
    return new ClientController(req, res)
}