import express from "express"
import cors from 'cors'
import * as bodyParser from "body-parser";
import * as swaggerUi from "swagger-ui-express"
import SuccessMessage from "./enum/SuccessMessage"
import { logger } from "./services/LoggerService"
import { routes } from "./routes";
import { ENV } from "./config/config"
import { sequelize } from "./config/sequelize"
import * as swaggerDocument from './config/swagger.json'

const app = express()

// Add a list of allowed origins.
// If you have more origins you would like to add, you can add them to the array below.
const allowedOrigins = [
  'http://localhost:4200',
  'http://localhost:4300',
  'http://localhost:8080'
]

const options: cors.CorsOptions = {
  origin: allowedOrigins
}

app.use(cors(options))

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json());
const port: number = ENV.API_PORT // default port to listen
const host: string = ENV.API_ADDRESS

sequelize
  .authenticate()
  .then(() => {
    // logger.info('Connection has been established successfully.');
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    // logger.info('Unable to connect to the database:', err);
    console.error('Unable to connect to the database:', err);
  });

// routes
// API-DOCS routes
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
// API routes
app.use("/api", routes)

// start the Express server
app.listen( port, host, () => {
    logger.info(SuccessMessage.SERVER_STARTED + ` 🚀 ${host}:${ port } 🚀`, __filename)
})

