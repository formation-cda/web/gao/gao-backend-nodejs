

import { authHelper } from '../helper/AuthHelper'
import { Response } from '../helper/Response'
import { Request } from '../helper/Request'

const auth = async (req: Request, res: Response, next: any) => {
    try {
      const token = authHelper.getTokenFromHeaders(req.headers)
      const decodedToken  = await authHelper.verifyToken(token)

      const response: any = {
        'success': false,
        'message': 'Accès non autoriosé! veillez vous authentifier',
        data: []
      }

      if(!token) return res.status(403).json(response);
      res.locals.tokenPlayload = decodedToken.playload
      next()
    } catch {
        res.status(401).json({
        error: new Error('Invalid request!')
      });
    }
}

const apiKeyAuth = async (req: Request, res: Response, next: any) => {
  try {
    const token = authHelper.getTokenFromHeaders(req.headers)
    const decodedToken  = await authHelper.verifyToken(token)

    const response: any = {
      'success': false,
      'message': 'Accès non autoriosé! veillez vous authentifier',
      data: []
    }

    if(!token) return res.status(403).json(response);
    res.locals.tokenPlayload = decodedToken.playload
    next()
  } catch {
      res.status(401).json({
      error: new Error('Invalid request!')
    });
  }
}

export default auth