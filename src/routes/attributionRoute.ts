import express from "express"
import { attributionController } from '../controller/AttibutionController'
import { Response } from '../helper/Response'
import { Request } from '../helper/Request'
import auth from "../middleware/AuthMiddleware"
const attributionRoute = express.Router()


attributionRoute.post('/', auth, (request: Request, response: Response) => {
  attributionController(request, response).add()
})

attributionRoute.post('/del', auth, (request: Request, response: Response) => {
  attributionController(request, response).delete()
})

export default attributionRoute  