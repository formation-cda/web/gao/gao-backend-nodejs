import express from "express"
import { clientController } from "../controller/ClientController"
import { Response } from '../helper/Response'
import { Request } from '../helper/Request'
import auth from '../middleware/AuthMiddleware'

const clientRoute = express.Router()

clientRoute.get('/:clientId', auth, (request: Request, response: Response) => {
  clientController(request, response).getClientById()
})

clientRoute.post('/search', auth, (request: Request, response: Response) => {
  clientController(request, response).search()
})

clientRoute.get('/', auth, (request: Request, response: Response) => {
  clientController(request, response).getClients()
})

clientRoute.post('/', auth, (request: Request, response: Response) => {
  clientController(request, response).add()
})

clientRoute.post('/update', auth, (request: Request, response: Response) => {
  clientController(request, response).update()
})

clientRoute.post('/delete', auth, (request: Request, response: Response) => {
  clientController(request, response).delete()
})


export default clientRoute