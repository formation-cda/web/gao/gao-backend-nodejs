import express from "express"
import { posteController } from '../controller/PosteController '
import { Response } from '../helper/Response'
import { Request } from '../helper/Request'
import auth from '../middleware/AuthMiddleware'

const posteRoute = express.Router()

posteRoute.get('/get', auth, (request: Request, response: Response) => {
  posteController(request, response).getPosteById()
})

posteRoute.get('/nb-poste', auth, (request: Request, response: Response) => {
  posteController(request, response).getNbPoste()
})

posteRoute.get('/list-ordi', auth, (request: Request, response: Response) => {
  posteController(request, response).getListOrdis()
})


posteRoute.post('/', auth, (request: Request, response: Response) => {
  posteController(request, response).add()
})

posteRoute.post('/update', auth, (request: Request, response: Response) => {
  posteController(request, response).update()
})

posteRoute.post('/delete', auth, (request: Request, response: Response) => {
  posteController(request, response).delete()
})

export default posteRoute