import express from "express"
import  userRoute  from './userRoute'
import  clientRoute  from './clientRoute'
import  posteRoute  from './posteRoute'
import  attributionRoute  from './attributionRoute'

const router = express.Router()

router.use('/',userRoute)
router.use('/clients',clientRoute)
router.use('/postes',posteRoute)
router.use('/attributions',attributionRoute)

export const routes = router