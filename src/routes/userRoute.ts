import express from "express"
import { userController } from '../controller/UserController'
import { Response } from '../helper/Response'
import { Request } from '../helper/Request'
import auth from '../middleware/AuthMiddleware'


const userRoute = express.Router()

userRoute.post('/register', (request: Request, response: Response) => {
  console.log(request.body)
  userController(request, response).register()
})

userRoute.post('/login', (request: Request, response: Response) => {
  userController(request, response).login()
})

export default userRoute