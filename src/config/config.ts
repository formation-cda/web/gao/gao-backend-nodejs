import * as dotenv from "dotenv";

dotenv.config();

export const ENV = {
    API_PORT: process.env.API_PORT ? Number(process.env.API_PORT) : 8080,
    API_ADDRESS: process.env.API_PORT ? process.env.API_ADDRESS : 'localhost',
    DB_NAME: process.env.DB_NAME ? process.env.DB_NAME : '',
    DB_USER: process.env.DB_USER ? process.env.DB_USER : '',
    DB_PASSWORD: process.env.DB_PASSWORD ? process.env.DB_PASSWORD : '',
    DB_HOST: process.env.DB_HOST ? process.env.DB_HOST : 'localhost',
    DB_DIALECT: process.env.DB_DIALECT ? process.env.DB_DIALECT : 'mysql',
    DB_NAME_DEV: process.env.DB_NAME_DEV ? process.env.DB_NAME_DEV : '',
    DB_USER_DEV: process.env.DB_USER_DEV ? process.env.DB_USER_DEV : '',
    DB_PASSWORD_DEV: process.env.DB_PASSWORD_DEV ? process.env.DB_PASSWORD_DEV : '',
    DB_HOST_DEV: process.env.DB_HOST_DEV ? process.env.DB_HOST_DEV : 'localhost',
    DB_DIALECT_DEV: process.env.DB_DIALECT_DEV ? process.env.DB_DIALECT_DEV : 'mysql',
    DB_NAME_TEST: process.env.DB_NAME_TEST ? process.env.DB_NAME_TEST : '',
    DB_USER_TEST: process.env.DB_USER_TEST ? process.env.DB_USER_TEST : '',
    DB_PASSWORD_TEST: process.env.DB_PASSWORD_TEST ? process.env.DB_PASSWORD_TEST : '',
    DB_HOST_TEST: process.env.DB_HOST_TEST ? process.env.DB_HOST_TEST : 'localhost',
    DB_DIALECT_TEST: process.env.DB_DIALECT_TEST ? process.env.DB_DIALECT_TEST : 'mysql',
    DB_PORT: process.env.DB_PORT ? Number(process.env.DB_PORT) : 3306,
    JWT_SECRET: process.env.JWT_SECRET ? process.env.JWT_SECRET : '',
    PWD_SALT: process.env.PWD_SALT ? Number(process.env.PWD_SALT) : 12,
}
