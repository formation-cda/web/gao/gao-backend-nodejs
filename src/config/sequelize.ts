import { Sequelize } from 'sequelize-typescript'
import { ENV } from './config'
import path from 'path'

const database:string = ENV.DB_NAME
const username:string = ENV.DB_USER
const password:string = ENV.DB_PASSWORD
const port:number = ENV.DB_PORT
const host:string = ENV.DB_HOST_DEV
const dialect:any = ENV.DB_DIALECT

export const sequelize = new Sequelize(
  database,
  username,
  password,
  {
    dialect,
    models: [path.join(__dirname ,'../../', '/src/models/*.model.ts'), path.join( __dirname, '../../', '/dist/models/*.model.js')],
    modelMatch: (filename, member) => {
      return filename.substring(0, filename.indexOf('.model')) === member.toLowerCase();
    },
    port,
    host
    /*dialectOptions: {
      socketPath: '/var/run/mysqld/mysqld.sock'
    }*/
  }
  
)
