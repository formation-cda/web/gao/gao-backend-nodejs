import { ENV } from '../config/config'
import * as jwt from 'jsonwebtoken'
import { Headers } from './Headers'

class AuthHelper {
    private readonly _jwtSecret = ENV.JWT_SECRET

    async createToken(playload: any) {
        if(!playload) return null
        try {
            const token:string = await jwt.sign({ userId: playload.userId, email: playload.email, role: playload.role }, this._jwtSecret)
            if (token) return token
            return null
        } catch (error) {
            return error
        }
    }

    verifyToken(token: string) {
        return new Promise((resolve, reject) => {
            jwt.verify(token, this._jwtSecret, async (err, decoded) => {
                if (err) {
                    resolve({playload: {}, verified: false })
                    return
                }
                resolve({playload: decoded, verified: true })
                return
            })
        }) as Promise<any>
    }

    async getPlayloadFromToken(token: string) {
        const { playload }: any = await this.verifyToken(token)
    }

    getTokenFromHeaders(headers: Headers) {
        const header = headers.authorization as string

        if (!header)
          return header

        return header.split('Bearer ')[1]
      }
}

export const authHelper = new AuthHelper()