import * as bcrypt from 'bcrypt'
import { ENV } from '../config/config'

class PasswordHelper {
    private readonly _saltRounds = ENV.PWD_SALT

    hashPassword = async (password: string) => {
        return await bcrypt.hash(password, this._saltRounds)
    }

    comparePassword = async (password: string, passwordHash: string) => {
        return bcrypt.compareSync( password, passwordHash);
    }
}

export const passwordHelper = new PasswordHelper()