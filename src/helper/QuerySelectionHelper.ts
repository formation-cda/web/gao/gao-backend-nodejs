export default class QuerySelectionHelper {
    private arrAttributes: any = null
    private includeField: any = []
    private excludeField: any = []
    private association: any = []

    join(model: any, attributes: any, condition: any = {}, innerJoin: boolean = false) { // model => type: Model, attributes => type: Array
        this.association.push({ model, attributes, where: condition , required: innerJoin })
        return this
    }

    nestJoin(parentModel: any, model: any, attributes: any, condition: any = {}, innerJoin: boolean = false) { // model => type: Model, attributes => type: Array
        // console.log("In QS - Parent : ", parentModel)
        // console.log("In QS - this.association : ", this.association)
        let assoc = this.association.filter((elem: any) => elem.model === parentModel).pop()
        // console.log("In QS - Assoc : ", assoc)
        assoc.include = [{ model, attributes , where: condition , required: innerJoin }]
        // console.log("In QS - Nested assoc : ", assoc)
        // this.association.push({ model, attributes , required: innerJoin })
        return this
    }

    include(data: any) { // data => type: Array
        this.includeField = data
        this.arrAttributes = this.includeField
        return this
    }

    exclude(data: any) { // data => type: Array
        this.excludeField = data
        this.arrAttributes = { exclude: this.excludeField }
        return this
    }

    attributes(){
        this.includeField = []
        this.excludeField = []
        return this.arrAttributes
    }

    endJoin() {
        const assoc = [...this.association]
        this.association = []
        return assoc
    }
}