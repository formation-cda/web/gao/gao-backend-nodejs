import { QueryInterface, Sequelize } from 'sequelize'
import { DataType } from 'sequelize-typescript'

export = {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /*
      Add altering commands here.

      Example:
      await queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
      await queryInterface.createTable('clients', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataType.INTEGER
        },
        lastName: {
          type:DataType.STRING,
          allowNull: true
        },
        firstName: {
          type:DataType.STRING,
          allowNull: false
        },
        createdAt: {
          type: DataType.DATE
        },
        updatedAt: {
          type: DataType.DATE
        },
        deletedAt: {
          type: DataType.DATE
        }
      });
  },

  down: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /*
      Add reverting commands here.

      Example:
      await queryInterface.dropTable('users');
    */
    await queryInterface.dropTable('clients');
  }
};
