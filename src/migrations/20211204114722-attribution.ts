import { QueryInterface, Sequelize } from 'sequelize'
import { DataType } from 'sequelize-typescript'

export = {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /*
      Add altering commands here.

      Example:
      await queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
      await queryInterface.createTable('attributions', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataType.INTEGER
        },
        date: {
          type:DataType.STRING,
          allowNull: true
        },
        horaire: {
          type:DataType.STRING,
          allowNull: false
        },
        posteId: {
          type: DataType.INTEGER,
          references: {
            model: {
              tableName: 'postes',
            },
            key: 'id'
          },
          allowNull: false
        },
        clientId: {
          type: DataType.INTEGER,
          references: {
            model: {
              tableName: 'clients',
            },
            key: 'id'
          },
          allowNull: false
        },
        createdAt: {
          type: DataType.DATE
        },
        updatedAt: {
          type: DataType.DATE
        },
        deletedAt: {
          type: DataType.DATE
        }
      });
  },

  down: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /*
      Add reverting commands here.

      Example:
      await queryInterface.dropTable('users');
    */
    await queryInterface.dropTable('attributions');
  }
};
