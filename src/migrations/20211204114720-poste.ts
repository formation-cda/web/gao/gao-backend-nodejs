import { QueryInterface, Sequelize } from 'sequelize'
import { DataType } from 'sequelize-typescript'

export = {
  up: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /*
      Add altering commands here.

      Example:
      await queryInterface.createTable('users', { id: Sequelize.INTEGER });
    */
      await queryInterface.createTable('postes', {
        id: {
          allowNull: false,
          autoIncrement: true,
          primaryKey: true,
          type: DataType.INTEGER
        },
        name: {
          type:DataType.STRING,
          allowNull: true
        },
        createdAt: {
          type: DataType.DATE
        },
        updatedAt: {
          type: DataType.DATE
        },
        deletedAt: {
          type: DataType.DATE
        }
      });
  },

  down: async (queryInterface: QueryInterface, sequelize: Sequelize) => {
    /*
      Add reverting commands here.

      Example:
      await queryInterface.dropTable('users');
    */
    await queryInterface.dropTable('postes');
  }
};
