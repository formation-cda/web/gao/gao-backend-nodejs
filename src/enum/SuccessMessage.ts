const SuccessMessage = {
    SUCCESS_CONNEXION: "Connexion effectué avec succès",
    SERVER_STARTED: "Démarrage du serveur effectué avec succès et écoute sur",
    SUCCESS_GET_CLIENTS: "Clients récupérés avec succès",
    SUCCESS_GET_USERS: "Utilisateurs récupérés avec succès",
    SUCCESS_GET_CLIENT: "Client récupéré avec succès",
    SUCCESS_GET_USER: "Utilisateur récupéré avec succès",
    SUCCESS_GET_POSTES: "Postes récupéré avec succès",
    SUCCESS_GET_POSTE: "Poste récupéré avec succès",
    SUCCESS_GET_POSTE_NB: "Nombre de poste récupéré avec succès",
    SUCCESS_USER_REGISTRATION: "Utilisateur enregistré avec succès",
    SUCCESS_CLIENT_ADD: "Client ajouté avec succès",
    SUCCESS_USER_LOGIN: 'Connexion effectué avec succès',
    SUCCESS_UPDATE_USER: "Utilisateur mise à jour avec succès",
    SUCCESS_DELETE_USER: "Suppression de l'utilisateur effectué avec succès",
    SUCCESS_UPDATE_CLIENT: "Client mise à jour avec succès",
    SUCCESS_DELETE_CLIENT: "Suppression du client effectué avec succès",
    SUCCESS_ADD_ATTRIBUTION: "Attribution effectué avec succès",
    SUCCESS_UPDATE_POSTE: "Poste mise à jour avec succès",
    SUCCESS_DELETE_POSTE: "Suppression du poste effectué avec succès",
    SUCCESS_POSTE_ADD: "Ajout de poste effectué avec succès",
    SUCCESS_DELETE_ATTRIBUTION: "Suppression de l'attribution effectué avec succès"
}

export default  SuccessMessage