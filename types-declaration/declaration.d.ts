
declare module 'LoggerService' 
declare module 'swagger-ui-express'
declare module 'validator'
declare module "*.json" {
    const value: any;
    export default value;
}