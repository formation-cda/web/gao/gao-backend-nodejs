import express from "express"
import * as bodyParser from 'body-parser';
import SuccessMessage from "./enum/SuccessMessage"
import { logger } from "./services/LoggerService"
import { json } from "body-parser";

const app = express()
app.use(bodyParser.json());
const port = 8080 // default port to listen

// define a route handler for the default home page
app.post( "/", ( req: any, res ) => {
    res.send( "Hello world!"+ JSON.stringify( req.body) )
} )



// start the Express server
app.listen( port, () => {
    // console.log( `server started at http://localhost:${ port }` )
    logger.info(SuccessMessage.SUCCESS_CONNEXION, __filename)
} )

