import BaseController from './BaseController'
import SuccessMessage from "../enum/SuccessMessage"
import { Request } from '../helper/Request'
import { Response } from '../helper/Response';

class PosteController extends BaseController {
    public index = (request: Request, response: Response) => {
        const clients = {
            nom: "HOLMES",
            prenom: "Sherlock Holmes"
        }

        this.sendResponse(response, [clients], SuccessMessage.SUCCESS_GET_CLIENTS)
    }
}

export const posteController = new PosteController()