import BaseController from './BaseController'
import SuccessMessage from "../enum/SuccessMessage"
import RequestR from '../helper/RequestR'
import Response from '../helper/ResponseR';

class ClientController extends BaseController {
    public index = (request: RequestR, response: Response) => {
        const clients = {
            nom: "HOLMES",
            prenom: "Sherlock Holmes"
        }

        this.sendResponse(response, [clients], SuccessMessage.SUCCESS_GET_CLIENTS)
    }
}

export const clientsController = new ClientController()