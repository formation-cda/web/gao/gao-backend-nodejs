class BaseController {
    public sendResponse = (res:any, result: any, message: string) => {
    	const response: any = {
            'success': true,
            'data'   : result,
            'message': message,
        }

        return res.json(response, 200);
    }

    public sendError = (res:any, error: any, errorMessages: any = [], code: number = 404) => {
    	let response: any = {
            'success': false,
            'message': error,
        }

        if(errorMessages){
            response['data'] = errorMessages;
        }

        return res.json(response, code);
    }
}