import BaseController from './BaseController'
import SuccessMessage from "../enum/SuccessMessage"

class ClientController extends BaseController {
    public index = (request: any, response: any) => {
        const clients = {
            nom: "HOLMES",
            prenom: "Sherlock Holmes"
        }

        this.sendResponse(response, [clients], SuccessMessage.SUCCESS_GET_CLIENTS)
    }
}

export const clientsController = new ClientController()