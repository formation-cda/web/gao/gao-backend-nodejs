import { ResponseR } from '../helper/ResponseR';

class BaseController {
    public sendResponse = (res:ResponseR, result: any, message: string) => {
    	const response: any = {
            'success': true,
            'data'   : result,
            'message': message,
        }

        return res.status(200).json(response);
    }

    public sendError = (res:ResponseR, error: any, errorMessages: any = [], code: number = 404) => {
    	let response: any = {
            'success': false,
            'message': error,
        }

        if(errorMessages){
            response['data'] = errorMessages;
        }

        return res.status(code).json(response);
    }
}

export default BaseController