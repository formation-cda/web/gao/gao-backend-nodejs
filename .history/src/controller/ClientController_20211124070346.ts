import BaseController from './BaseController'
import SuccessMessage from "../enum/SuccessMessage"
import { Request } from '../helper/RequestR'
import { Response } from '../helper/Response';

class ClientController extends BaseController {
    public index = (request: Request, response: Response) => {
        const clients = {
            nom: "HOLMES",
            prenom: "Sherlock Holmes"
        }

        this.sendResponse(response, [clients], SuccessMessage.SUCCESS_GET_CLIENTS)
    }
}

export const clientsController = new ClientController()