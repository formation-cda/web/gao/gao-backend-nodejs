// import express from "express"

// const app = express()
// const port = 8080 // default port to listen

// // define a route handler for the default home page
// app.get( "/", ( req, res ) => {
//     res.send( "Hello world!" )
// } )

// // start the Express server
// app.listen( port, () => {
//     // console.log( `server started at http://localhost:${ port }` )
// } )

import express from 'express'
import SuccessMessage from './enum/SuccessMessage'
import { logger } from './services/LoggerService'

const app = express()

logger.info(SuccessMessage.SUCCESS_CONNEXION, __filename)
logger.log("Test", __filename)

// console.log('Hello World\!\!\!')
