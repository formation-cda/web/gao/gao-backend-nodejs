import express from "express"
import * as bodyParser from 'body-parser';
import SuccessMessage from "./enum/SuccessMessage"
import { logger } from "./services/LoggerService"
import { routes } from "./routes";
import from "./config/config"

const app = express()
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json());
const port = process.env.API_PORT ? process.env.API_PORT : 8080 // default port to listen

// routes
app.use('/api', routes)

// start the Express server
app.listen( port, () => {
    // console.log( `server started at http://localhost:${ port }` )
    logger.info(SuccessMessage.SUCCESS_CONNEXION, __filename)
} )

