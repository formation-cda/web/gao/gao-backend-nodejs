const SuccessMessage = {
    SUCCESS_CONNEXION: "Connexion effectué avec succès",
    SERVER_STARTED: "Démarrage du serveur effectué avec succès et écoute sur",
    SUCCESS_GET_CLIENTS: "Clients récupérés avec succès"
}

export default  SuccessMessage