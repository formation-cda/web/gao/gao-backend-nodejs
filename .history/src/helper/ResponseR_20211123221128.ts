import {IncomingMessage} from "http";

class ResponseR {       
    public resp: IncomingMessage

    constructor(resp: IncomingMessage) {
        this.resp = resp
    }   

    public static instance(resp: IncomingMessage){
        return new ResponseR(resp)
    }
}

export default ResponseR