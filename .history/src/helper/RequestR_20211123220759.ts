import * as express from 'express';

class RequestR {       
    public req: express.Request

    constructor(req: express.Request) {
        this.req = req
    }   

    public static instance(req: express.Request){
        return new RequestR(req)
    }
}

export default RequestR