import {IncomingMessage} from "http";

class RequestR {       
    public req: IncomingMessage

    constructor(req: IncomingMessage) {
        this.req = req
    }   

    public static instance(req: IncomingMessage){
        return new RequestR(req)
    }
}

export default RequestR