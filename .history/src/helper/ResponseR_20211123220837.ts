import * as express from 'express';

class ResponseR {       
    public resp: express.Response

    constructor(resp: express.Response) {
        this.resp = resp
    }   

    public static instance(resp: express.Response){
        return new ResponseR(resp)
    }
}

export default ResponseR