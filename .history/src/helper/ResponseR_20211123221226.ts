import { ServerResponse } from "http";

class ResponseR {       
    public resp: ServerResponse

    constructor(resp: ServerResponse) {
        this.resp = resp
    }   

    public static instance(resp: ServerResponse){
        return new ResponseR(resp)
    }
}

export default ResponseR