import express from "express"
import https from 'https'
import fs from 'fs'
import * as bodyParser from 'body-parser';
import SuccessMessage from "./enum/SuccessMessage"
import { logger } from "./services/LoggerService"
import { routes } from "./routes";
import { ENV } from "./config/config"

const app = express()
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json());
const port: number = ENV.API_PORT // default port to listen
const host: string = ENV.API_ADDRESS

// const key = fs.readFileSync(__dirname + '/../certs/selfsigned.key');
// const cert = fs.readFileSync(__dirname + '/../certs/selfsigned.crt');
// const options = {
//   key: key,
//   cert: cert
// };

// routes
app.use('/api', routes)

// start the Express server
// const server = https.createServer(options, app);
// server.listen( port, host, () => {
//     // console.log( `server started at ${host}:${ port }` )
//     logger.info(SuccessMessage.SERVER_STARTED + ` ${host}:${ port }`, __filename)
// })

app.listen( port, host, () => {
    // console.log( `server started at ${host}:${ port }` )
    logger.info(SuccessMessage.SERVER_STARTED + ` 🚀 ${host}:${ port } 🚀`, __filename)
})


