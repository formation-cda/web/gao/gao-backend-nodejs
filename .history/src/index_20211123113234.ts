import express from "express"
import SuccessMessage from "./enum/SuccessMessage"
import { logger } from "./services/LoggerService"

const pinoMiddleware = logger.expressPinoLogger

const app = express()
const port = 8080 // default port to listen

// // // define a route handler for the default home page
// // app.get( "/", ( req, res ) => {
// //     res.send( "Hello world!" )
// // } )

// // middleware that augments the req - must be added before the pino middleware
// app.use((req: any, res, next) => {
//     req.user = 'testing';
//     next();
//   })

// app.use(pinoMiddleware)
var ExpressPinoLogger = require('express-pino-logger')()

var pino = ExpressPinoLogger({
  serializers: {
    req: (req) => ({
      method: req.method,
      url: req.url,
      user: req.raw.user,
    }),
  },
})

// middleware that augments the req - must be added before the pino middleware
app.use((req, res, next) => {
  req.user = 'testing';
  next();
})

app.use(pino)

// start the Express server
app.listen( port, () => {
    // console.log( `server started at http://localhost:${ port }` )
    logger.info(SuccessMessage.SUCCESS_CONNEXION)
} )

