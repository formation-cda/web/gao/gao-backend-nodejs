import express from "express"
const posteRoute = express.Router()

posteRoute.get('/', (request, response) => {
  response.send('Hello world! Poste')
})

posteRoute.get('/test', (request, response) => {
  response.send('Hello world! Test Poste')
})

export default posteRoute