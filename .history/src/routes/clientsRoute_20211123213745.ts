import express from "express"
import ClientsController from '../controller/ClientController'

const clientsRoute = express.Router();

const clientsController =  new ClientsController()
clientsRoute.get('/', clientsController.index)

clientsRoute.get('/test', (request, response) => {
  response.send('Hello world! Test Client')
})

export default clientsRoute  