import express from "express"
const attributionRoute = express.Router();

attributionRoute.get('/', (request, response) => {
  response.send('Hello world! Client');
});

attributionRoute.get('/test', (request, response) => {
  response.send('Hello world! Test Client');
});

export default attributionRoute  