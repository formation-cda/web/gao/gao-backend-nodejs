import express from "express"
const attributionRoute = express.Router();

attributionRoute.get('/', (request, response) => {
  response.send('Hello world! Attribution');
});

attributionRoute.get('/test', (request, response) => {
  response.send('Hello world! Test AAAttribution');
});

export default attributionRoute  