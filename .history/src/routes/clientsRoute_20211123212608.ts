import express from "express"
const clientsRoute = express.Router();

clientsRoute.get('/', (request, response) => {
  response.send('Hello world! Client')
})

clientsRoute.get('/test', (request, response) => {
  response.send('Hello world! Test Client')
})

export default clientsRoute  