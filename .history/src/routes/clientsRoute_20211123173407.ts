import express from "express"
const router = express.Router();

router.get('/', (request, response) => {
  response.send('Hello world! Client');
});

router.get('/test', (request, response) => {
  response.send('Hello world! Test Client');
});

export const clientsRoute = router 