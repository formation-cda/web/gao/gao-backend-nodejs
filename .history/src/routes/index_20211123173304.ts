import express from "express"
import { clientsRoute } from './clientsRoute';
import { posteRoute } from './posteRoute';
const router = express.Router();

router.use('/clients',clientsRoute)
router.use('/postes',posteRoute)

export default router