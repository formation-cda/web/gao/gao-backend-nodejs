import express from "express"
import  clientsRoute  from './clientsRoute'
import  posteRoute  from './posteRoute'
import  attributionRoute  from './attributionRoute'

const router = express.Router()

router.use('/clients',clientsRoute)
router.use('/postes',posteRoute)
router.use('/attributions',attributionRoute)

export const routes = router