import express from "express"
import { clientsController } from '../controller/ClientController'

const clientsRoute = express.Router();

clientsRoute.get('/', clientsController.index)

clientsRoute.get('/test', (request, response) => {
  response.send('Hello world! Test Client')
})

export default clientsRoute  