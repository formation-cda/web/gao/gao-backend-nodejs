import express from "express"
import { clientController } from '../controller/ClientController'

const clientsRoute = express.Router();

clientsRoute.get('/', clientController.index)

clientsRoute.get('/test', (request, response) => {
  response.send('Hello world! Test Client')
})

export default clientsRoute  