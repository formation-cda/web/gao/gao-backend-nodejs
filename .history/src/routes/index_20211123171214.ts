import { clientsRoute } from './clientsRoute';
import { posteRoute } from './posteRoute';

const router = {
  clientsRoute,
  posteRoute
}

export default router