import express from "express"
import * as bodyParser from 'body-parser';
import SuccessMessage from "./enum/SuccessMessage"
import { logger } from "./services/LoggerService"
import * as routes from './routes/index';

const app = express()
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json());
const port = 8080 // default port to listen

// routes
app.use('/', routes)



// start the Express server
app.listen( port, () => {
    // console.log( `server started at http://localhost:${ port }` )
    logger.info(SuccessMessage.SUCCESS_CONNEXION, __filename)
} )

