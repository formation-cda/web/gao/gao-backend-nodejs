import express from "express"
import ErrorMessage from "./enum/ErrorMessage"
import SuccessMessage from "./enum/SuccessMessage"
import { logger } from "./services/LoggerService"

const app = express()
const port = 8080 // default port to listen

// define a route handler for the default home page
app.get( "/", ( req, res ) => {

    logger.error(ErrorMessage.WRONG_LOGIN_OR_PASSWORD)
    res.send( "Hello world!" )
} )

// start the Express server
app.listen( port, () => {
    // console.log( `server started at http://localhost:${ port }` )

    logger.info(SuccessMessage.SUCCESS_CONNEXION)
} )

