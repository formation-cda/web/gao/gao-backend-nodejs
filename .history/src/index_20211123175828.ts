import express from "express"
import * as bodyParser from 'body-parser';
import SuccessMessage from "./enum/SuccessMessage"
import { logger } from "./services/LoggerService"
import { routes } from "./routes";
import { ENV } from "./config/config"

const app = express()
// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))
// parse application/json
app.use(bodyParser.json());
const port: number = ENV.API_PORT // default port to listen
const host: string = ENV.API_ADDRESS

// routes
app.use('/api', routes)

// start the Express server
app.listen( port, host, () => {
    // console.log( `server started at ${host}:${ port }` )
    logger.info(SuccessMessage.SUCCESS_CONNEXION + ` server started at ${host}:${ port }`, __filename)
})

