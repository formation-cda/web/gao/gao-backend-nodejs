import pino from 'pino'
import ExpressPinoLogger from 'express-pino-logger'

class LoggerService {

  static info = (message: string = "", _filename: string = "") => {
    this.displayLog(message, _filename, "info")
  }

  static error = (message: string = "", _filename: string = "") => {
    this.displayLog(message, _filename, "error")
  }

  static warn = (message: string = "", _filename: string = "") => {
    this.displayLog(message, _filename, "warn")
  }

  static log = (message: string = "", _filename: string = "") => {
    const content = { message, fichier: (_filename !== "" ? this.getFileRelativePath(_filename) : "Fichier non spécifié.")}
    this.logToFile.info(content)
  }

  static displayLog = (message: string, _filename: string, level: string) => {
    const content =  "\n Message : " + message + " \n Fichier : " + (_filename !== "" ? this.getFileRelativePath(_filename) : "Fichier non spécifié.")
    this.logger[level](content)
  }

  static logger = pino(
    {
        transport: {
            target: "pino-pretty",
            options: {
                translateTime: "SYS: dd-mm-yyyy HH:MM:ss",
                ignore: "pid, hostname"
            }
        }
    },
  )

  static logToFile = pino(
    {
        level: "info",
        name: "testttttt",
        transport: {
            target: "pino-pretty",
            options: {
                translateTime: "SYS: dd-mm-yyyy HH:MM:ss",
                ignore: "pid, hostname",
                hideObject: false, // --hideObject
                destination: process.cwd()+"/logs/app.log"
            }
        }
    },
  )

  static expressPinoLogger = ExpressPinoLogger()

  static getFileRelativePath = (_filename: string = "")=> {
    return ( _filename.includes("dist") ? _filename.split("/dist").pop() : _filename.split("/src").pop())
  }
}

export const logger = LoggerService