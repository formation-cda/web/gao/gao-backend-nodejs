import pino from 'pino'
import ExpressPinoLogger from 'express-pino-logger'
import path from 'path'


class LoggerService {

  static info = (message: string = "", _filename: string = "") => {
    this.displayLog(message, _filename, "info")
  }

  static error = (message: string = "", _filename: string = "") => {
    this.displayLog(message, _filename, "error")
  }

  static warn = (message: string = "", _filename: string = "") => {
    this.displayLog(message, _filename, "warn")
  }

  static log = (message: string = "", _filename: string = "") => {
    const fileName = this.getFileRelativePath(_filename)
    const content = { message, fichier: ((fileName && fileName !== "") ? fileName : "Fichier non spécifié.")}
    this.logToFile.info(content)
  }

  static displayLog = (message: string, _filename: string, level: string) => {
    const fileName = this.getFileRelativePath(_filename)
    const content =  "\n Message : " + message + " \n Fichier : " + ((fileName && fileName !== "") ? fileName : "Fichier non spécifié.")
    this.logger[level](content)
  }

  static logger = pino(
    {
        transport: {
            target: "pino-pretty",
            options: {
                translateTime: "SYS: dd-mm-yyyy HH:MM:ss",
                ignore: "pid, hostname",
                // hideObject: false, // --hideObject
            }
        }
    },
  )

  static logToFile = pino(
    {
        level: "info",
        transport: {
            target: "pino-pretty",
            options: {
                translateTime: "SYS: dd-mm-yyyy HH:MM:ss",
                ignore: "pid, hostname",
                hideObject: false, // --hideObject
                destination: process.cwd()+"/logs/app.log",
            }
        }
    },
  )

  static expressPinoLogger = ExpressPinoLogger({
    serializers: {
      req: (req: any) => ({
        method: req.method,
        url: req.url,
        user: req.raw.user,
      }),
    },
  })

  static getFileRelativePath = (_filename: string)=> {
    const fileName = path.basename(_filename)
    console.log(_filename.split("/src"))
    const relativePath = _filename.includes("dist") ? _filename.split("/dist").pop() : _filename.split("/src").pop()
    return relativePath + "/" + fileName
  }
}

export const logger = LoggerService