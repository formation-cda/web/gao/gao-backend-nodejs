
import * as dotenv from "dotenv";

dotenv.config();

export const ENV = {
    API_PORT: process.env.API_PORT ? process.env.API_PORT : 8080
}
